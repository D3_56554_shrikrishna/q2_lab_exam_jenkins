const express = require('express')
const utils = require('../utils')
const{query} = require('express')

const router = express.Router()

router.get('get/:name',(request,response) => {
    const{name} = request.params
    const statement = ` select *from Movie where movie_name = '${name}'`
    const connection = utils.openConnection()

    connection.query(statement,(error,result)=>{
        connection.end()
        response.send(utils.createResult(error,result))
    })
})

router.post('/add',(request,response) => {
    const{movie_title,movie_release_date,movie_time,director_name} = request.body
    const statement = `insert into Movie(movie_title,movie_release_date,movie_time,director_name)
    values('${movie_title}','${movie_release_date}','${movie_time}','${director_name}')`
    const connection = utils.openConnection()

    connection.query(statement,(error,result)=>{
        connection.end()
        response.send(utils.createResult(error,result))
    })
})

router.put('/update/:name',(request,response) => {
    const{name} = request.params
    const{movie_title,movie_release_date,movie_time,director_name} = request.body
    const statement = `update Movie set movie_release_date='${movir_release_date}',
    movie_time='${movie_time}' where movie_title='${name}'`
    const connection = utils.openConnection()

    connection.query(statement,(error,result)=>{
        connection.end()
        response.send(utils.createResult(error,result))
    })
})

router.delete('/delete/:name',(request,response) => {
    const{name} = request.params
    const statement = ` delete from Movie where movie_name = '${name}'`
    const connection = utils.openConnection()

    connection.query(statement,(error,result)=>{
        connection.end()
        response.send(utils.createResult(error,result))
    })
})



module.exports = router